cd ~/service
ip=`curl http://checkip.amazonaws.com`
restart=0
curl -k https://ctrl.skyssr.top:8988/api/ssr/$ip > user-config.json 
updated=`diff user-config.json  shadowsocksr/user-config.json  | wc -l`
if [ "$updated" != "0" ]; then
echo "New password. update it and restart process"
cp user-config.json shadowsocksr/
restart=1
else
echo "the password is same"
fi

curl -k https://ctrl.skyssr.top:8988/api/ssr/$ip?cmd=tun > go-config.tmp
updated=`diff go-config.tmp go-config.json | wc -l`
if [ "$updated" != "0" ]; then
echo "New tun password. update it and restart process"
cp go-config.tmp go-config.json
sudo cp go-config.tmp /etc/shadowsocks-libev/config.json
restart=1
else
echo "the tun password is same"
fi

if [ "$restart" == "1" ]; then
sudo ./restart.sh
fi
