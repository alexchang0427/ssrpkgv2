#!/bin/bash
flag=$1
dt=`date "+%F %H:%M"` 
dt2=`date +%Y%m%d`
log=/home/ubuntu/service/www/web/traffic.txt
if [ "$flag" == "end" ]; then
log=/home/ubuntu/service/www/web/traffic-end.txt
fi
if [ "$flag" == "start" ]; then
log=/home/ubuntu/service/www/web/traffic-start.txt
fi
ifn=`sudo route | grep '^default' | grep -o '[^ ]*$'`
#traffic=`sudo ifconfig  $ifn  | grep "RX bytes"`
rx_bytes=`cat /sys/class/net/$ifn/statistics/rx_bytes`
tx_bytes=`cat /sys/class/net/$ifn/statistics/tx_bytes`
echo $dt" Traffic: RX bytes:$rx_bytes, TX bytes:$tx_bytes" > $log
