#!/usr/bin/env python
# Reflects the requests from HTTP methods GET, POST, PUT, and DELETE
# Written by Nathan Hamiel (2010)
import SocketServer
import socket
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from optparse import OptionParser
import logging
from logging.handlers import TimedRotatingFileHandler
import subprocess
import ssl
import thread
import time

rootdir = "/home/ubuntu/service/www/web"
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create a file handler
#handler = logging.FileHandler('/home/ubuntu/service/www/log/fakehttp.log')
handler = TimedRotatingFileHandler('/home/ubuntu/service/www/log/fakehttps.log',
                                       when="d",
                                       interval=1,
                                       backupCount=5)
handler.setLevel(logging.DEBUG)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)


class MyHttpServer(HTTPServer):
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)

    def start_server(self):
        self.serve_forever()

class MyHttpsServer(HTTPServer):
    def server_bind(self):
        self.socket = ssl.wrap_socket (self.socket, certfile='/home/ubuntu/service/www/server.pem', server_side=True)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)

    def start_server(self):
        self.serve_forever()

class RequestHandler(BaseHTTPRequestHandler):
    def log_message(self, format, *args):
        logger.info("%s - - [%s] %s" % (self.address_string(),self.log_date_time_string(),format%args))

    def do_GET(self):
        request_path = self.path

        logger.debug("----- Request Start ----->")
        logger.debug(request_path)
        logger.debug(self.headers)
        logger.debug("<----- Request End -----")
        try:
            if self.path.endswith('500.html') or self.path.endswith('mon.txt') or self.path.endswith('traffic.txt') or self.path.endswith('traffic-end.txt')  or self.path.endswith('traffic-start.txt') :
                f = open(rootdir + self.path) #open requested file

                #send code 200 response
                self.send_response(200)
                self.send_header("Set-Cookie", "iot_service=1")

                #send header first
                self.send_header('Content-type','text-html')
                self.end_headers()

                #send file content to client
                self.wfile.write(f.read())
                f.close()
                return

            if self.path.endswith('restart_ssr52292.cmd'):
                self.send_header('Content-type','text-html')
                self.send_error(404, "OK")
                subprocess.call ('/home/ubuntu/service/restart.sh&', shell=True)
                return

            self.send_response(500)
            self.send_header("Set-Cookie", "iot_service=1")
        except IOError:
            self.send_error(404, 'file not found')
            self.send_header("Set-Cookie", "iot_service=1")

    def do_POST(self):
        #logger.info("POST from %s \n" % (self.address_string))
        request_path = self.path

        logger.debug("----- Request Start ----->")
        logger.debug(request_path)

        request_headers = self.headers
        content_length = request_headers.getheaders('content-length')
        length = int(content_length[0]) if content_length else 0

        logger.debug(request_headers)
        logger.debug(self.rfile.read(length))
        logger.debug("<----- Request End -----")

        self.send_response(404)

    do_PUT = do_POST
    do_DELETE = do_GET

class ThreadingSimpleSSLServer(SocketServer.ThreadingMixIn,
                   MyHttpsServer):
    pass

class ThreadingSimpleServer(SocketServer.ThreadingMixIn,
                   MyHttpServer):
    pass

def start_http():
    port = 80
    logger.info('Listening on http://localhost:%s' % port)
    #server = HTTPServer(('', port), RequestHandler)
    #server = MyHttpServer(('', port), RequestHandler, bind_and_activate=False)
    server = ThreadingSimpleServer(('', port), RequestHandler, bind_and_activate=False)
    server.allow_reuse_address = True
    server.server_bind()
    server.server_activate()
    server.serve_forever()

def start_http8143():
    port = 8143
    logger.info('Listening on http://localhost:%s' % port)
    #server = HTTPServer(('', port), RequestHandler)
    #server = MyHttpServer(('', port), RequestHandler, bind_and_activate=False)
    server = ThreadingSimpleServer(('', port), RequestHandler, bind_and_activate=False)
    server.allow_reuse_address = True
    server.server_bind()
    server.server_activate()
    server.serve_forever()

def start_https():
    port = 443
    logger.info('Listening on https://localhost:%s' % port)
    #server = HTTPServer(('', port), RequestHandler)
    #server = MyHttpServer(('', port), RequestHandler, bind_and_activate=False)
    serverSSL = ThreadingSimpleSSLServer(('', port), RequestHandler, bind_and_activate=False)
    serverSSL.allow_reuse_address = True
    serverSSL.server_bind()
    serverSSL.server_activate()
    serverSSL.serve_forever()

def main():
    thread.start_new_thread(start_http, ())
    thread.start_new_thread(start_http8143, ())
    thread.start_new_thread(start_https, ())

    while 1:
        time.sleep(10)

if __name__ == "__main__":
    parser = OptionParser()
    parser.usage = ("Creates an http-server that will echo out any GET or POST parameters\n"
                    "Run:\n\n"
                    "   reflect")
    (options, args) = parser.parse_args()

    main()
