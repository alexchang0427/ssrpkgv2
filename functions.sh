#!/bin/bash
TOPDIR=`pwd`
is14=0
username=`whoami`
is_ubuntu=`awk -F= '/^NAME/{print $2}' /etc/os-release | grep Ubuntu | wc -l`
is_centos=`awk -F= '/^NAME/{print $2}' /etc/os-release | grep CentOS | wc -l`
if [ "$is_ubuntu" = "1" ]; then
is14=`uname -a | grep "14.04" | wc -l`
fi

function add_ubuntu_user()
{
  if [ "$is_ubuntu" = "1" -a  "$username" != "ubuntu" ]; then
    # add new user
    sudo useradd -m -p `openssl passwd -1 sky@123$%^` ubuntu -G sudo -s /bin/bash
    sleep 1
    sudo echo "export PATH=\$PATH:/sbin:/usr/sbin" >> /home/ubuntu/.bashrc 
  fi

  if [ "$is_centos" = "1" -a  "$username" != "ubuntu" ]; then
    # add new user
    sudo useradd -m -p `openssl passwd -1 sky@123$%^` ubuntu -G wheel -s /bin/bash
    sleep 1
    sudo echo "export PATH=\$PATH:/sbin:/usr/sbin" >> /home/ubuntu/.bashrc 
  fi
}

function enable_ufw()
{
sudo ufw allow ssh
sudo ufw allow 443
sudo ufw allow 8143
sudo ufw allow 993
sudo ufw allow 80
sudo ufw allow 995
sudo ufw allow 465
sudo ufw allow 21
sudo ufw allow 5061
sudo ufw allow 6514
sudo ufw allow 554
sudo ufw allow 9418
sudo ufw allow 9999
sudo ufw allow 63530/udp
sudo ufw allow 8888/udp
sudo ufw allow 8889/udp
sudo ufw allow 8890/udp
sudo ufw allow 9999/udp
sudo ufw allow 80:400/tcp
sudo ufw allow 80:400/udp
sudo ufw enable
}

function block_bittorrent()
{
sudo sh -c 'echo "127.0.0.1   0d.kebhana.mx"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   1337.abcvg.info"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   9.rarbg.to"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   agusiq-torrents.pl"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   bt.xxx-tracker.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   denis.stalker.upeer.me"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   exodus.desync.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   explodie.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   ipv4.tracker.harry.lu"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   open.acgnxtracker.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   open.demonii.si"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   open.stealth.si"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   open.trackerlist.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   opentracker.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   packages.crunchbangplusplus.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   peersteers.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   pubt.in"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   retracker.lanta-net.ru"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   retracker.mgts.by"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   share.camoe.cn"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   t.nyaatracker.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   thetracker.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   torrent.nwps.ws"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   torrentclub.tech"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.coppersurfer.tk"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.cyberia.is"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.dler.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.dyn.im"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.fastcast.nz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.fastdownload.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.filepit.to"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.gbitt.info"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.iamhansen.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.internetwarriors.net"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.justseed.it"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.kamigami.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.leechers-paradise.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.moeking.me"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.novg.net"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.nyaa.uk"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.open-tracker.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.opentrackr.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.openwebtorrent.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.port443.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.swateam.org.uk"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.tfile.me.php"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.tiny-vps.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.torrent.eu.org"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.torrentyorg.pl"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.uw0.xyz"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker.vectahosting.eu"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker1.itzmx.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker1.wasabii.com.tw"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker2.itzmx.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker3.itzmx.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   tracker4.itzmx.com"  >> /etc/hosts'
sudo sh -c 'echo "127.0.0.1   zephir.monocul.us"  >> /etc/hosts'
}

function setup_timezone()
{
  
  if [ "$is_ubuntu" = "1" ]; then
    sudo dpkg-reconfigure tzdata
  fi
  if [ "$is_centos" = "1" ]; then
    sudo timedatectl set-timezone Asia/Shanghai 
  fi
}

function make_pkg_directory()
{
  mkdir -p  /home/ubuntu/install
  mkdir -p  /home/ubuntu/service
  mkdir -p  /home/ubuntu/service/xkcptun
  mkdir -p  /home/ubuntu/service/log
  mkdir -p  /home/ubuntu/service/www/web 
  mkdir -p  /home/ubuntu/service/www/log
}

function copy_pre_pkg_files()
{
cp pkg/sssvc_setup.sh /home/ubuntu/install/sssvc_setup.sh
cp pkg/update_cfg.sh /home/ubuntu/service
cp pkg/user-config.json /home/ubuntu/service/user-config.json
cp pkg/go-config.json /home/ubuntu/service
cp pkg/xkcp_server /home/ubuntu/service/xkcptun
cp pkg/xkcptun.json /home/ubuntu/service/xkcptun
cp pkg/xkcptun_8889.json /home/ubuntu/service/xkcptun
cp pkg/xkcptun_8890.json /home/ubuntu/service/xkcptun
#cp go_ss_bin.tar.gz /home/ubuntu/
cp pkg/superbench.sh  /home/ubuntu/service/superbench.sh
cp pkg/check_ip.sh  /home/ubuntu/service/check_ip.sh
cp pkg/lastip.sh    /home/ubuntu/service/lastip.sh
cp pkg/speedtest.py  /home/ubuntu/service/speedtest.py
cp pkg/fakehttp.py   /home/ubuntu/service/www/fakehttp.py
cp pkg/server.pem   /home/ubuntu/service/www/server.pem
cp pkg/maketraffic.sh  /home/ubuntu/service/maketraffic.sh
cp pkg/makemon.sh  /home/ubuntu/service/makemon.sh
cp pkg/restart.sh  /home/ubuntu/service/restart.sh
echo "Just installed." > /home/ubuntu/service/www/web/mon.txt
echo "index.html" > /home/ubuntu/service/www/web/index.html
cp pkg/web/*   /home/ubuntu/service/www/web -rf
ifname=`route | grep default | awk '{print $8}'`
if [ "$1" == "dhcp" ]; then
#ifname=`route | grep default | awk '{print $8}'`
sed -i s/enp2s0/$ifname/g /home/ubuntu/service/check_ip.sh
fi
sed -i s/eth0/$ifname/g /home/ubuntu/service/xkcptun/xkcptun.json
sed -i s/eth0/$ifname/g /home/ubuntu/service/xkcptun/xkcptun_8889.json
sed -i s/eth0/$ifname/g /home/ubuntu/service/xkcptun/xkcptun_8890.json 
chown ubuntu:ubuntu /home/ubuntu/install/ /home/ubuntu/service/ -R
}

function init_ubuntu_service()
{
if [ "$is14" == "0" ]; then
sudo cp linux/ubuntu/service/fakeweb.service /etc/systemd/system/fakeweb.service
sudo cp linux/ubuntu/service/xkcptun.service /etc/systemd/system/xkcptun.service
sudo cp linux/ubuntu/service/shadowsocksr.service /etc/systemd/system/shadowsocksr.service
sudo cp linux/ubuntu/service/xkcptun_8889.service /etc/systemd/system/xkcptun_8889.service
sudo cp linux/ubuntu/service/xkcptun_8890.service /etc/systemd/system/xkcptun_8890.service
#sudo cp linux/ubuntu/service/shadowsocksgo.service /etc/systemd/system/shadowsocksgo.service
sudo systemctl enable shadowsocksr.service
#sudo systemctl enable shadowsocksgo.service
sudo systemctl enable fakeweb.service
sudo systemctl enable xkcptun.service
sudo systemctl enable xkcptun_8889.service
sudo systemctl enable xkcptun_8890.service
else
#sudo cp linux/ubuntu/service/shadowsocksgo.conf  /etc/init/
#sudo ln -s /etc/init/shadowsocksgo.conf /etc/init.d/shadowsocksgo
sudo cp linux/ubuntu/service/shadowsocksr.conf  /etc/init/
sudo ln -s /etc/init/shadowsocksr.conf /etc/init.d/shadowsocksr

sudo cp linux/ubuntu/service/xkcptun.conf  /etc/init/
sudo ln -s /etc/init/xkcptun.conf /etc/init.d/xkcptun

sudo cp linux/ubuntu/service/fakeweb.conf /etc/init/
sudo ln -s /etc/init/fakeweb.conf /etc/init.d/fakeweb

sudo cp linux/ubuntu/service/xkcptun_8889.conf /etc/init/
sudo ln -s /etc/init/xkcptun_8889.conf  /etc/init.d/xkcptun_8889

sudo cp linux/ubuntu/service/xkcptun_8890.conf /etc/init/
sudo ln -s /etc/init/xkcptun_8890.conf  /etc/init.d/xkcptun_8890

sudo add-apt-repository ppa:chris-lea/libsodium;
sudo echo "deb http://ppa.launchpad.net/chris-lea/libsodium/ubuntu trusty main" >> /etc/apt/sources.list;
sudo echo "deb-src http://ppa.launchpad.net/chris-lea/libsodium/ubuntu trusty main" >> /etc/apt/sources.list;
fi

# change max file
sudo cp linux/ubuntu/limits.conf /etc/security/limits.conf
# Install Google BBR and Optimize the Server
#wget --no-check-certificate https://github.com/teddysun/across/raw/master/bbr.sh
#chmod +x bbr.sh
#sudo ./bbr.sh
sudo cp linux/ubuntu/sysctl.conf  /etc/sysctl.conf
#reboot
sudo sysctl -p 
}

function init_ubuntu_sstun_service()
{
if [ "$is14" == "0" ]; then
sudo cp linux/ubuntu/service/fakeweb.service /etc/systemd/system/fakeweb.service
sudo systemctl enable fakeweb.service
else
sudo cp linux/ubuntu/service/fakeweb.conf /etc/init/
sudo ln -s /etc/init/fakeweb.conf /etc/init.d/fakeweb
sudo add-apt-repository ppa:chris-lea/libsodium;
sudo sh -c 'echo "deb http://ppa.launchpad.net/chris-lea/libsodium/ubuntu trusty main" >> /etc/apt/sources.list;'
sudo sh -c 'echo "deb-src http://ppa.launchpad.net/chris-lea/libsodium/ubuntu trusty main" >> /etc/apt/sources.list;'
fi

# change max file
sudo cp linux/ubuntu/limits.conf /etc/security/limits.conf
sudo cp linux/ubuntu/sysctl.conf  /etc/sysctl.conf
sudo sysctl -p 
}

function init_centos_service()
{
sudo cp linux/ubuntu/service/fakeweb.service /etc/systemd/system/fakeweb.service
sudo cp linux/ubuntu/service/xkcptun.service /etc/systemd/system/xkcptun.service
sudo cp linux/ubuntu/service/shadowsocksr.service /etc/systemd/system/shadowsocksr.service
sudo cp linux/ubuntu/service/xkcptun_8889.service /etc/systemd/system/xkcptun_8889.service
sudo cp linux/ubuntu/service/xkcptun_8890.service /etc/systemd/system/xkcptun_8890.service
#sudo cp linux/ubuntu/service/shadowsocksgo.service /etc/systemd/system/shadowsocksgo.service
sudo systemctl enable shadowsocksr.service
#sudo systemctl enable shadowsocksgo.service
sudo systemctl enable fakeweb.service
sudo systemctl enable xkcptun.service
sudo systemctl enable xkcptun_8889.service
sudo systemctl enable xkcptun_8890.service
}


function init_centos_sstun_service()
{
sudo cp linux/ubuntu/service/fakeweb.service /etc/systemd/system/fakeweb.service
sudo systemctl enable fakeweb.service
}

function init_sslib_cfg_json()
{
sudo sh -c '
echo "
{
        \"server\": \"0.0.0.0\",
        \"server_port\": 63530,
        \"local_port\": 1080,
        \"password\": \"ssr63520\",
        \"method\": \"aes-256-cfb\",
        \"timeout\": 90,
        \"workers\":10
}" > /etc/shadowsocks-libev/config.json
'
}

function init_sslib_cfg_env()
{
if [ -e /etc/default/shadowsocks-libev ]; then
cat /etc/default/shadowsocks-libev | sudo sh -c 'sed "s/^DAEMON_ARGS.*/DAEMON_ARGS=\"-U --reuse-port --fast-open --no-delay\"/" > /etc/default/shadowsocks-libev'
else
cat /etc/init.d/shadowsocks-libev | sudo sh - c 'sed "s/^DAEMON_ARGS.*/DAEMON_ARGS=\"-U --reuse-port --fast-open --no-delay\"/" > /etc/init.d/shadowsocks-libev'
fi

}

function install_apt_pkgs()
{
if [ "$is_ubuntu" = "1" ]; then
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:max-c-lv/shadowsocks-libev -y
echo "Waiting add-apt-repository and updating for shadowsocks-libev........"
sleep 10
sudo apt-get update
sleep 20
sudo apt-get -y install ufw
sudo apt-get -y install python
sudo apt-get -y install python-pip
sudo apt-get -y install curl
sudo apt-get -y install libevent-2.0 
#sudo apt-get -y install golang-go
sudo apt-get -y install git

if [ "$is14" == "0" ]; then
sudo apt-get -y install libsodium18
else
sudo apt-get -y install libsodium-dev
fi

sudo apt-get -y install shadowsocks-libev
sleep 10
init_sslib_cfg_json
init_sslib_cfg_env
sudo systemctl enable  shadowsocks-libev
sudo service shadowsocks-libev restart 

#mkdir go
#export GOPATH=/home/ubuntu/go
#echo "export GOPATH=/home/ubuntu/go" >> ~/.bashrc
#echo "export PATH=\$PATH:/home/ubuntu/go/bin" >> ~/.bashrc
#go get github.com/shadowsocks/shadowsocks-go/cmd/shadowsocks-server

cd /home/ubuntu/
#tar xvf go_ss_bin.tar.gz
mkdir -p service
cd service
git clone -b manyuser https://github.com/shadowsocksrr/shadowsocksr.git
sudo ln -s /home/ubuntu/service/shadowsocksr /usr/local/

chown ubuntu:ubuntu /home/ubuntu/install/ /home/ubuntu/service/ -R
fi
}

function install_apt_sstun_pkgs()
{
if [ "$is_ubuntu" = "1" ]; then
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:max-c-lv/shadowsocks-libev -y
echo "Waiting add-apt-repository and updating for shadowsocks-libev........"
sudo apt-get update
sleep 20
sudo apt-get -y install ufw
sudo apt-get -y install python
sudo apt-get -y install python-pip
sudo apt-get -y install curl
sudo apt-get -y install libevent-2.0 
#sudo apt-get -y install golang-go
sudo apt-get -y install git

if [ "$is14" == "0" ]; then
sudo apt-get -y install libsodium18
else
sudo apt-get -y install libsodium-dev
fi

sudo apt-get -y install shadowsocks-libev
sleep 10
init_sslib_cfg_json
init_sslib_cfg_env
sudo systemctl enable  shadowsocks-libev
sudo service shadowsocks-libev restart 

cp $TOPDIR/pkg/update_cfg_sstun.sh /home/ubuntu/service/update_cfg.sh
cp $TOPDIR/pkg/restart_sstun.sh  /home/ubuntu/service/restart.sh

cd /home/ubuntu/
sudo chown ubuntu:ubuntu /home/ubuntu/install/ /home/ubuntu/service/ -R
fi
}

function install_yum_pkgs()
{
if [ "$is_centos" = "1" ]; then
#sudo yum update
sudo yum -y install epel-release
sudo yum -y install ufw
sudo yum -y install python-pip
sudo yum -y install curl
sudo yum -y install wget
sudo yum -y install git
sudo systemctl enable ufw
sudo systemctl start ufw
wget https://copr.fedorainfracloud.org/coprs/librehat/shadowsocks/repo/epel-7/librehat-shadowsocks-epel-7.repo
sudo mv librehat-shadowsocks-epel-7.repo /etc/yum.repos.d/
sudo yum -y update
sudo yum -y install libevent
sudo yum -y install libsodium
sudo yum -y install libevent-devel
sudo yum -y install shadowsocks-libev
sudo systemctl enable shadowsocks-libev.service
sudo systemctl start shadowsocks-libev.service
cd /home/ubuntu/
#tar xvf go_ss_bin.tar.gz
mkdir -p service
cd service
git clone -b manyuser https://github.com/shadowsocksrr/shadowsocksr.git
sudo ln -s /home/ubuntu/service/shadowsocksr /usr/local/

sudo chown ubuntu:ubuntu /home/ubuntu/install/ /home/ubuntu/service/ -R 
fi
}


function install_yum_sstun_pkgs()
{
if [ "$is_centos" = "1" ]; then
#sudo yum update
sudo yum -y install epel-release
sudo yum -y install ufw
sudo yum -y install python
sudo yum -y install python-pip
sudo yum -y install curl
sudo yum -y install wget
sudo yum -y install git
sudo systemctl enable ufw
sudo systemctl start ufw
wget https://copr.fedorainfracloud.org/coprs/librehat/shadowsocks/repo/epel-7/librehat-shadowsocks-epel-7.repo
sudo mv librehat-shadowsocks-epel-7.repo /etc/yum.repos.d/
sudo yum -y update
sudo yum -y install libevent
sudo yum -y install libsodium
sudo yum -y install libevent-devel
sudo yum -y install shadowsocks-libev
init_sslib_cfg_json
init_sslib_cfg_env
sudo systemctl enable  shadowsocks-libev
sudo service shadowsocks-libev restart 

cp $TOPDIR/pkg/update_cfg_sstun.sh /home/ubuntu/service/update_cfg.sh
cp $TOPDIR/pkg/restart_sstun.sh  /home/ubuntu/service/restart.sh

cp pkg/restart_sstun.sh  /home/ubuntu/service/restart.sh

cd /home/ubuntu/
sudo chown ubuntu:ubuntu /home/ubuntu/install/ /home/ubuntu/service/ -R
fi
}


function copy_post_pkg_files()
{
echo "empty copy_post_pkg_files"
# /etc/sysconfig/shadowsocks-libev
if [ "$is_centos" = "1" ]; then 
sudo cp $TOPDIR/linux/centos/sysconfig/shadowsocks-libev /etc/sysconfig/shadowsocks-libev
fi

if [ "$is_ubuntu" = "1" ]; then
sudo cp $TOPDIR/linux/ubuntu/default/shadowsocks-libev /etc/default/shadowsocks-libev
fi
}

function init_crond()
{
(echo "10 * * * * sudo service fakeweb restart ") | sudo crontab -
if [ "$1" == "dhcp" ]; then 
(sudo crontab -l ; echo "*/5 * * * * /bin/bash /home/ubuntu/service/check_ip.sh") | sudo crontab -
fi
(sudo crontab -l ; echo "*/5 * * * * /bin/bash /home/ubuntu/service/makemon.sh") | sudo crontab -
(sudo crontab -l ; echo "1 * * * * /bin/bash /home/ubuntu/service/maketraffic.sh start") | sudo crontab -
(sudo crontab -l ; echo "59 * * * * /bin/bash /home/ubuntu/service/maketraffic.sh end") | sudo crontab -
#(sudo crontab -l ; echo "0 4 * * * /bin/bash /home/ubuntu/service/restart.sh") | sudo crontab -

# AWS 
if [ "$username" != "ubuntu" ]; then
#(sudo crontab -l ; echo "0 4 * * * sudo reboot") | sudo crontab -
(sudo crontab -l ; echo "0 4 * * * /bin/bash /home/ubuntu/service/restart.sh") | sudo crontab -
(sudo crontab -l ; echo "0 0 1 * * sudo reboot") | sudo crontab -
else
(sudo crontab -l ; echo "0 4 * * * /bin/bash /home/ubuntu/service/restart.sh") | sudo crontab -
(sudo crontab -l ; echo "0 0 1 * * sudo reboot") | sudo crontab -
fi
}

function init_crond_sstun()
{
(echo "10 * * * * sudo service fakeweb restart ") | sudo crontab -
if [ "$1" == "dhcp" ]; then
(sudo crontab -l ; echo "*/5 * * * * /bin/bash /home/ubuntu/service/check_ip.sh") | sudo crontab -
fi
(sudo crontab -l ; echo "*/5 * * * * /bin/bash /home/ubuntu/service/makemon.sh") | sudo crontab -
(sudo crontab -l ; echo "1 * * * * /bin/bash /home/ubuntu/service/maketraffic.sh start") | sudo crontab -
(sudo crontab -l ; echo "59 * * * * /bin/bash /home/ubuntu/service/maketraffic.sh end") | sudo crontab -
(sudo crontab -l ; echo "0 4 * * * service shadowsocks-libev restart") | sudo crontab -
}


function setup_timezone()
{
  
  if [ "$is_ubuntu" = "1" ]; then
    #sudo dpkg-reconfigure tzdata
    sudo timedatectl set-timezone Asia/Shanghai 
  fi
  if [ "$is_centos" = "1" ]; then
    sudo timedatectl set-timezone Asia/Shanghai 
  fi
}

